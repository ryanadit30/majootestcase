import 'package:flutter/material.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';

class Preference {
  static const USER_INFO = "user-info";

}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class UIHelper {

  static ProgressDialog? progressDialogShow(BuildContext context, ProgressDialog? pd){
    final _mediaHeight = MediaQuery.of(context).size.height;
    final _mediaWidth = MediaQuery.of(context).size.width;
    pd = ProgressDialog(context: context);
    pd.show(
      max: 100,
      msg: 'Mohon Tunggu...',
      borderRadius: _mediaWidth*0.001,
      backgroundColor: Colors.white,
      progressValueColor: Colors.tealAccent.shade700,
      elevation: 5.0,
      barrierDismissible: true,
      msqFontWeight: FontWeight.w500,
      msgFontSize: _mediaHeight*0.025,
    );
    return pd;
  }

}

enum ModePage {
  Login,
  Register
}
