import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state)
    {
      final retryBtn = Column(
        children: [
          Icon(Icons.refresh_sharp),
          SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: (){
              context.read<HomeBlocCubit>().fetchingData();
            },
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                padding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 15
                )
            ),
            child: Text("REFRESH",
              style: TextStyle(
                  fontSize: 10,
                  color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      );

      if(state is HomeBlocLoadedState)
        {
          return HomeBlocLoadedScreen(data: state.data);
        }
      else if(state is HomeBlocLoadingState)
        {
          return LoadingIndicator(
            color: Colors.lightGreen,
            height: 5,
          );
        }

      else if(state is HomeBlocInitialState)
        {
          return Scaffold();
        }

      else if(state is HomeBlocErrorState)
        {
          return ErrorScreen(
            message: state.error,
            retryButton: retryBtn,
            retry: () {},
          );
        }
      else if(state is HomeBlocNetworkErrorState)
        {
          return ErrorScreen(
            message: state.error,
            retryButton: retryBtn,
            retry: () {},
          );
        }

      return Scaffold(
        body: Center(child: Text(
            kDebugMode?"state not implemented $state": ""
        )),
      );
    });
  }
}
