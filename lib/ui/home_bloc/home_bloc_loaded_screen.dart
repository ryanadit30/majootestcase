import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data!.length,
        itemBuilder: (context, index) {
          return movieItemWidget(context,data![index]);
        },
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Data? data){
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (_) => DetailMovie(
            data: data,
          )
        )).then((value) => context.read<HomeBlocCubit>().fetchingData());
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)
            )
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network(data!.i!.imageUrl!),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.l!, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }

}

class DetailMovie extends StatelessWidget {
  final Data? data;
  const DetailMovie({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final space = SizedBox(height: 10,);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(25),
          child: Column(
            children: [
              Image.network(data!.i!.imageUrl!),
              space,
              _buildTextTitle("Title Movie"),
              Text(data!.l!, textDirection: TextDirection.ltr),
              space,
              _buildTextTitle("Tahun Movie"),
              Text(data!.year!.toString(), textDirection: TextDirection.ltr),
              space,
              data!.series != null ? _buildListSeries() : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTextTitle(String text){
    return Container(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 10
      ),
      child: Text("$text :",
        style: TextStyle(
          color: Colors.grey.shade800,
          fontWeight: FontWeight.w600
        ),
      ),
    );
  }

  Widget _buildListSeries() {
    List<Widget> listWidget = data!.series!.map((e) {
      return ListTile(
        title: Text("${e.l}",
          textDirection: TextDirection.ltr,
        ),
        leading: Image.network(e.i!.imageUrl!),
        contentPadding: EdgeInsets.symmetric(
          vertical: 5
        ),
      );
    }).toList();
    
    return Container(
      child: Column(
        children: [
          _buildTextTitle("List Series"),
          Column(
            children: listWidget,
          )
        ],
      ),
    );
  }

}



