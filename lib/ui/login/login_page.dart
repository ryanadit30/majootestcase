import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';


class LoginPage extends StatefulWidget {

  final ModePage? modePage;

  LoginPage({
    Key? key,
    this.modePage
  }) : super(key: key);

  @override
  _LoginState createState() => _LoginState(modePage: modePage);
}

class _LoginState extends State<LoginPage> {
  ModePage? modePage = ModePage.Login;
  _LoginState({this.modePage});
  ProgressDialog? pd;
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  final TextController? _emailController = TextController(initialValue: "");
  final TextController? _passwordController = TextController(initialValue: "");
  final TextController? _userNameController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _emailController?.dispose();
    _passwordController?.dispose();
    _userNameController?.dispose();
  }

  @override
  Widget build(BuildContext context) {

    String buttonStr = "LOGIN";
    String descHeadTitle = "Silahkan login terlebih dahulu";
    if(modePage == ModePage.Register) {
      buttonStr = "REGISTER";
      descHeadTitle = "Silahkan daftar member";
    }

    return Scaffold(
      body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
        listenWhen: (context, state) {
          if(state is AuthBlocLoggedInState)return true;
          else if(state is AuthBlocRegisteredState)return true;
          else if(state is AuthBlocFailedLoginState) return true;
          else if(state is AuthBlocFailedRegisterState)return true;
          else return false;
        },
        listener: (context, state) {
          if(state is AuthBlocLoggedInState){
            if(pd != null)pd!.close();
            _showToast("Login Berhasil", Colors.lightGreen);
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetchingData(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
          else if(state is AuthBlocFailedLoginState){
            if(pd != null)pd!.close();
            _showToast("Login Gagal, Email atau password salah", Colors.red.shade400);
          }
          else if(state is AuthBlocRegisteredState){
            if(pd != null)pd!.close();
            _showToast("Registrasi Berhasil", Colors.lightGreen);
            Navigator.of(context).pop();
          }
          else if(state is AuthBlocFailedRegisterState){
            if(pd != null)pd!.close();
            _showToast("Registrasi Gagal, email sudah digunakan", Colors.red.shade400);
          }
        },
        builder: (context, state) => SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                top: 75,
                left: 25,
                bottom: 25,
                right: 25
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  '$descHeadTitle',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: '$buttonStr',
                  onPressed: () {
                    handleButton(context);
                  },
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                if(modePage == ModePage.Login)_register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          if(modePage == ModePage.Register)CustomTextFormField(
            context: context,
            label: 'Username',
            hint: 'username',
            controller: _userNameController,
            validator: (val) {
              return val!.trim().isNotEmpty ? null : "username harap diisi";
            },
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          // TODO : TO CLEAR FORM
          _passwordController?.textController.clear();
          _emailController?.textController.clear();
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => BlocProvider(
                create: (context) => AuthBlocCubit(),
                child: RegisterPage(),
              )
            )
          );
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleButton(BuildContext context) {
    if(modePage == ModePage.Login) {
      handleLogin(context);
    }else {
      handleRegister(context);
    }
  }

  void handleLogin(BuildContext context) async {
    String? _email = _emailController?.value;
    String? _password = _passwordController?.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null
       ) {
      if(_email.trim().isNotEmpty && _password.trim().isNotEmpty) {
        pd = UIHelper.progressDialogShow(context, pd);
        User user = User(
          email: _email,
          password: _password,
        );
        context.read<AuthBlocCubit>().loginUser(user);
        //authBlocCubit.login_user(user);
      }
      else {
        _showToast("Form tidak boleh kosong, mohon cek kembali data yang anda inputkan", Colors.red.shade400);
      }

    }

  }

  void handleRegister(BuildContext context) async {
    String? _email = _emailController?.value;
    String? _password = _passwordController?.value;
    String? _userName = _userNameController?.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null && _userName != null
    ) {
      if(_email.trim().isNotEmpty && _password.trim().isNotEmpty && _userName.trim().isNotEmpty) {
        pd = UIHelper.progressDialogShow(context, pd);
        User user = User(
            email: _email,
            password: _password,
            userName: _userName
        );
        context.read<AuthBlocCubit>().registerUser(user);
      }else {
        _showToast("Form tidak boleh kosong, mohon cek kembali data yang anda inputkan", Colors.red.shade400);
      }

    }

  }

  void _showToast(String text, Color? bgColor) {
    final toast = SnackBar(
      content: Text("$text",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      backgroundColor: bgColor,
    );
    ScaffoldMessenger.of(context).showSnackBar(toast);
  }

}


class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LoginPage(
      modePage: ModePage.Register,
    );
  }
}
