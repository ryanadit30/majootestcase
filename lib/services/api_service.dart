import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/error_helper.dart';

enum StatusGetMovieList {
  SuccessLoaded,
  FailLoaded,
  NetworkFailLoaded
}

class ApiServices{

  StatusGetMovieList _statusGetMovieList = StatusGetMovieList.FailLoaded;
  StatusGetMovieList get getStatusGetMovieList => _statusGetMovieList;
  String? _messageErrorGetMovieResponse;
  String? get getMessageErrorGetMovie => _messageErrorGetMovieResponse;

  Future<MovieResponse?> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response  = await dio!.get("");
      MovieResponse? movieResponse = MovieResponse.fromJson(jsonDecode(response.data!));
      _statusGetMovieList = StatusGetMovieList.SuccessLoaded;
      return movieResponse;
    } on DioError catch(e) {
      _statusGetMovieList = ErrorHelper.getTypeError(e);
      _messageErrorGetMovieResponse = ErrorHelper.extractApiError(e);
      return null;
    } catch (e) {
      _statusGetMovieList = StatusGetMovieList.FailLoaded;
      _messageErrorGetMovieResponse = ErrorHelper.getErrorMessage(e);
      return null;
    }
  }
}