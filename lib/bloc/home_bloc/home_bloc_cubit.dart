import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocInitialState());
    try{
      emit(HomeBlocLoadingState());
      ApiServices apiServices = ApiServices();
      MovieResponse? movieResponse = await apiServices.getMovieList();
      if(apiServices.getStatusGetMovieList == StatusGetMovieList.SuccessLoaded) {
        emit(HomeBlocLoadedState(movieResponse?.data));
      } else if(apiServices.getStatusGetMovieList == StatusGetMovieList.NetworkFailLoaded) {
        emit(HomeBlocNetworkErrorState("${apiServices.getMessageErrorGetMovie}"));
      } else {
        emit(HomeBlocErrorState("${apiServices.getMessageErrorGetMovie}"));
      }
    } catch (e) {
      print(e);
    }
  }
}
