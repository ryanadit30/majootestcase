import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());
  Database? _database;
  Future<Database> get database async => _database ??= await _initDBUser();

  void fetchHistoryLogin() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn == null){
      emit(AuthBlocLoginState());
    }else{
      if(isLoggedIn){
        emit(AuthBlocLoggedInState());
      }else{
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async{
   try {
     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
     emit(AuthBlocLoadingState());
     Database dB = await database;
     final emailMatch = await dB.query("user", where: "id = ?", whereArgs: [user.email] );
     await Future<void>.delayed(const Duration(seconds: 1));
     if(emailMatch.length > 0) {
       emailMatch.forEach((element) async{
         final usr = User.fromJson(element);
         print(usr.password);
         if(usr.password == user.password) {
           await sharedPreferences.setBool("is_logged_in",true);
           String data = user.toJson().toString();
           sharedPreferences.setString("user_value", data);
           emit(AuthBlocLoggedInState());
         } else {
           await sharedPreferences.setBool("is_logged_in",false);
           emit(AuthBlocFailedLoginState());
         }
       });
     } else {
       await sharedPreferences.setBool("is_logged_in",false);
       emit(AuthBlocFailedLoginState());
     }

   } catch(e) {
     emit(AuthBlocFailedLoginState());
     print(e);

   }
  }

  void registerUser(User user) async {
    try {
      emit(AuthBlocLoadingState());
      Database dB = await database;
      await dB.insert("user", User(
          email: user.email,
          password: user.password,
          userName: user.userName
      ).toMap() );
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(AuthBlocRegisteredState());
    } catch(e) {
      print(e);
      emit(AuthBlocFailedRegisterState());
    }
  }

  Future<Database> _initDBUser() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'movie.db');
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          // When creating the db, create the table
          await db.execute(
              '''
      CREATE TABLE user(
          id TEXT PRIMARY KEY,
          email TEXT,
          password TEXT,
          username TEXT
      )
      ''');
        });
  }

}
